use std::io;

fn parse_play(play: &str) -> Option<i32> {
    match play {
        "X" | "A" => Some(1),
        "Y" | "B" => Some(2),
        "Z" | "C" => Some(3),
        _ => None,
    }
}

fn main () {
    let res: i32 = io::stdin().lines().map(|line| {
        let v: Vec<i32> = line.unwrap().split(" ").map(|x| parse_play(x).unwrap()).collect();
        v[1] + if v[0] == v[1] { 3 } else if v[1] == v[0] % 3 + 1 { 6 } else { 0 }
    }).sum();
    println!("{}", res);
}
