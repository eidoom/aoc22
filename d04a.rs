use std::io;

mod d04l;

fn main () {
    let cnt = io::stdin().lines().map(|l| l.unwrap()).filter(|line| {
        let val = d04l::parse_line(line);
        (val[0] <= val[2] && val[1] >= val[3])
            || (val[0] >= val[2] && val[1] <= val[3])
    }).count();
    println!("{}", cnt);
}
