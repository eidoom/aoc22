use std::io;

fn main () {
    println!("{}",
        io::stdin().lines().fold((0, 0), |(cur, max), line| {
            match line.unwrap().as_str() {
                "" => (0, if cur > max { cur } else { max }),
                val => (cur + val.parse::<i32>().unwrap(), max),
            }
        }).1
    );
}
