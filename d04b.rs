use std::{io, cmp};

mod d04l;

fn main () {
    let cnt = io::stdin().lines().map(|l| l.unwrap()).filter(|line| {
        let val = d04l::parse_line(line);
        cmp::min(val[1], val[3]) >= cmp::max(val[0], val[2])
    }).count();
    println!("{}", cnt);
}
