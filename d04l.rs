pub fn parse_line(line: &String) -> Vec<i32> {
    line.split(&[',', '-'][..]).map(|x|
        x.parse::<i32>().unwrap()
    ).collect()
}
