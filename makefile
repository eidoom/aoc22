PRG_RS = $(basename $(wildcard *[ab].rs))
PRG_CPP=$(addsuffix _cpp, $(basename $(wildcard *[ab].cpp)))

RS=rustc --edition 2021 -O
CPP=g++ -std=c++20

.PHONY: rs cpp all clean

all: rs cpp

cpp: $(PRG_CPP)

rs: $(PRG_RS)

clean:
	-rm $(PRG_RS) $(PRG_CPP)

%: %.rs
	$(RS) $< -o $@

d04%: d04%.rs d04l.rs
	$(RS) $< -o $@

d06%: d06%.rs d06l.rs
	$(RS) $< -o $@

%_cpp: %.cpp
	$(CPP) $< -o $@
