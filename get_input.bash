#!/usr/bin/env bash

for f in d*a.*; do
	nn=${f#d}
	nn=${nn%a.*}
	n=${nn#0}
	o=i${nn}
	if [ ! -f $o ]; then
		curl --output ${o} --header "Cookie: session=$(cat session)" https://adventofcode.com/2022/day/${n}/input
	fi
done
