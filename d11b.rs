use std::{
    io::stdin,
    collections::VecDeque,
};

fn main () {
    let mut inspections: Vec<u64> = vec![];
    let mut itemss: Vec<VecDeque<u64>> = vec![];
    let mut ops: Vec<Box<dyn Fn(u64) -> u64>> = vec![];
    let mut tests: Vec<u64> = vec![];
    let mut yes: Vec<usize> = vec![];
    let mut no: Vec<usize> = vec![];
    let mut i = 0;
    for line in stdin().lines().map(|l| l.unwrap()) {
        i = (i + 1) % 7;
        match i {
            1 => inspections.push(0),
            2 => {
                let items: VecDeque<u64> = (&line[18..]).split(", ").map(|x| x.parse().unwrap()).collect();
                itemss.push(items);
            },
            3 => {
                let op = if let Ok(n) = (&line[25..]).parse::<u64>() {
                    match &line[23..24] {
                        "*" => Some(Box::new(move |x| x * n) as Box<dyn Fn(u64) -> u64>),
                        "+" => Some(Box::new(move |x| x + n) as Box<dyn Fn(u64) -> u64>),
                        _ => None,
                    }
                } else {
                    match &line[23..24] {
                        "*" => Some(Box::new(move |x| x * x) as Box<dyn Fn(u64) -> u64>),
                        "+" => Some(Box::new(move |x| x + x) as Box<dyn Fn(u64) -> u64>),
                        _ => None,
                    }
                }.unwrap();
                ops.push(op);
            },
            4 => {
                let n = (&line[21..]).parse().unwrap();
                tests.push(n);
            },
            5 => {
                let n = (&line[29..]).parse().unwrap();
                yes.push(n);
            },
            6 => {
                let n = (&line[30..]).parse().unwrap();
                no.push(n);
            },
            _ => (),
        }
    }
    let field = tests.iter().fold(1, |acc, x| acc * x);
    // println!("{}", field);
    for _ in 0..10000 {
        for i in 0..itemss.len() {
            // println!("monkey {}", i);
            while let Some(mut item) = itemss[i].pop_front() {
                inspections[i] += 1;
                // println!("{}", item);
                item = ops[i](item) % field;
                // println!("{}", item);
                if item % tests[i] == 0 {
                    // println!("-> {}", yes[i]);
                    itemss[yes[i]].push_back(item);
                } else {
                    // println!("-> {}", no[i]);
                    itemss[no[i]].push_back(item);
                }
                // println!("");
            }
        }
        // println!("{:?}", itemss);
    }
    // println!("{:?}", inspections);
    inspections.sort_unstable();
    let m = inspections.len();
    println!("{:?}", inspections[m-1] * inspections[m-2]);
}
