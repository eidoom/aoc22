use std::io;

fn main () {
    println!("{}",
        io::stdin().lines().map(|line| {
            let val = line.unwrap();
            let half = val.len() / 2;
            let dup = val[..half].chars().find(|&c|
                val[half..].chars().any(|d| d == c)
            ).unwrap();
            let d = dup as i32 - 96;
            if d < 0 { d + 58 } else { d }
        }).sum::<i32>()
    );
}
