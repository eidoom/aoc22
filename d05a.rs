use std::io;

fn main () {
    let lines: Vec<String> = io::stdin().lines()
        .map(|l| l.unwrap()).collect();

    let mut secs = lines.split(|l| l.is_empty());
    let mut pre = secs.next().unwrap().iter().rev();

    let l: usize = pre.next().unwrap()
        .split_ascii_whitespace().rev().next().unwrap()
        .parse().unwrap();
    let mut stacks: Vec<Vec<char>> = vec![vec![]; l];

    for line in pre {
        for (i, c) in line.chars().skip(1).step_by(4).enumerate() {
            match c {
                a @ 'A'..='Z' => stacks[i].push(a),
                _ => {},
            }
        }
    }

    for m in secs.next().unwrap() {
        let v: Vec<usize> = m.split_ascii_whitespace().skip(1).step_by(2)
            .map(|x| x.parse::<usize>().unwrap()).collect();
        for _ in 0..v[0] {
            let c = stacks[v[1]-1].pop().unwrap();
            stacks[v[2]-1].push(c);
        }
    }
    let a: String = stacks.iter().map(|s| s[s.len()-1]).collect();

    println!("{}", a);
}
