use std::{
    collections::HashSet,
    io::stdin,
};

fn main () {
    let mut head: [i32; 2] = [0, 0]; // [x, y]
    let mut tail: [i32; 2] = [0, 0];
    let mut all = HashSet::from([tail]);

    for line in stdin().lines().map(|l| l.unwrap()) {
        let n: u32 = line[2..].parse().unwrap();

        let (i, s) = match &line[..1] {
            "R" => Some((0,  1)),
            "L" => Some((0, -1)),
            "U" => Some((1,  1)),
            "D" => Some((1, -1)),
            _ => None,
        }.unwrap();

        for _ in 0..n {
            head[i] += s;

            let dx = head[0] - tail[0];
            let dy = head[1] - tail[1];
            if (dx.abs() > 1 && dy.abs() == 1) || (dx.abs() == 1 && dy.abs() > 1) {
                tail[0] += dx.signum();
                tail[1] += dy.signum();
            }
            else if dx.abs() > 1 {
                tail[0] += dx.signum();
            }
            else if dy.abs() > 1 {
                tail[1] += dy.signum();
            }

            all.insert(tail);
        }
    }
    println!("{}", all.len());
}
