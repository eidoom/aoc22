#!/usr/bin/env bash

test_part() {
	if [[ "$(./d$1$2 < i$1)" == "$3" ]]; then
		echo d$1$2 passed
	else
		echo d$1$2 failed
	fi
}

test_day() {
	test_part $1 a $2
	test_part $1 b $3
}

make -j

test_day 01 72017 212520
test_day 02 8392 10116
test_day 03 7908 2838
test_day 04 485 857
test_day 05 ZSQVCCJLL QZFJRWHGS
test_day 06 1779 2635
test_day 07 1084134 6183184
test_day 08 1859 332640
test_day 09 6011 2419
test_part 10 a 15140
test_day 11 54253 13119526120
test_day 12 497 492
