use std::{
    io::stdin,
};

fn main () {
    let mut inspections: Vec<u32> = vec![];
    let mut itemss: Vec<Vec<u32>> = vec![];
    let mut ops: Vec<Box<dyn Fn(u32) -> u32>> = vec![];
    let mut tests: Vec<u32> = vec![];
    let mut yes: Vec<usize> = vec![];
    let mut no: Vec<usize> = vec![];
    let mut i = 0;
    for line in stdin().lines().map(|l| l.unwrap()) {
        i = (i + 1) % 7;
        match i {
            1 => inspections.push(0),
            2 => {
                let items: Vec<u32> = (&line[18..]).split(", ").map(|x| x.parse().unwrap()).collect();
                itemss.push(items);
            },
            3 => {
                let op = if let Ok(n) = (&line[25..]).parse::<u32>() {
                    let tmp: Option<Box<dyn Fn(u32) -> u32>> = match &line[23..24] {
                        "*" => Some(Box::new(move |x| x * n)),
                        "+" => Some(Box::new(move |x| x + n)),
                        _ => None,
                    };
                    tmp
                } else {
                    let tmp: Option<Box<dyn Fn(u32) -> u32>> = match &line[23..24] {
                        "*" => Some(Box::new(move |x| x * x)),
                        "+" => Some(Box::new(move |x| x + x)),
                        _ => None,
                    };
                    tmp
                }.unwrap();
                ops.push(op);
            },
            4 => {
                let n = (&line[21..]).parse().unwrap();
                tests.push(n);
            },
            5 => {
                let n = (&line[29..]).parse().unwrap();
                yes.push(n);
            },
            6 => {
                let n = (&line[30..]).parse().unwrap();
                no.push(n);
            },
            _ => (),
        }
    }
    for _ in 0..20 {
        for i in 0..itemss.len() {
            // println!("monkey {}", i);
            while let Some(mut item) = itemss[i].pop() {
                inspections[i] += 1;
                // println!("{}", item);
                item = ops[i](item);
                // println!("{}", item);
                item /= 3;
                // println!("{}", item);
                if item % tests[i] == 0 {
                    // println!("-> {}", yes[i]);
                    itemss[yes[i]].push(item);
                } else {
                    // println!("-> {}", no[i]);
                    itemss[no[i]].push(item);
                }
                // println!("");
            }
        }
        // println!("{:?}", itemss);
    }
    // println!("{:?}", itemss);
    inspections.sort_unstable();
    let m = inspections.len();
    println!("{:?}", inspections[m-1] * inspections[m-2]);
}
