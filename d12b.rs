use std::{
    io::stdin,
    collections::BinaryHeap,
    cmp::Ordering,
};

#[derive(Eq, PartialEq)]
struct Path{
    cost: usize,
    pos: (usize, usize),
}

impl Ord for Path {
    fn cmp(&self, other: &Self) -> Ordering {
        other.cost.cmp(&self.cost)
    }
}

impl PartialOrd for Path {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn main () {
    let mut i0 = 0;
    let mut j0 = 0;

    let map: Vec<Vec<usize>> = stdin().lines().map(|l| l.unwrap()).enumerate().map(|(i, line)| {
        line.chars().enumerate().map(|(j, c)| {
            (match c {
                'S' => {
                    'a'
                },
                'E' => {
                    i0 = i;
                    j0 = j;
                    'z'
                },
                _  => c,
            }) as usize - 97
        }).collect()
    }).collect();

    let w = map[0].len();
    let h = map.len();
    // println!("{} {}", w, h);

    let mut dist = vec![vec![usize::MAX; w]; h];
    dist[i0][j0] = 0;

    // YAGNI
    // Dijkstra: https://doc.rust-lang.org/std/collections/binary_heap/index.html
    // min-heap: https://doc.rust-lang.org/std/collections/struct.BinaryHeap.html#min-heap
    // cf https://gitlab.com/eidoom/aoc21/-/blob/main/d15.py
    let mut queue = BinaryHeap::new();
    queue.push(Path{ cost: 0, pos: (i0, j0) });

    while let Some(Path{ cost, pos: (i, j) }) = queue.pop() {

        let lim = map[i][j] - 1;
        let mut edges = vec![];
        if j > 0 && map[i][j-1] >= lim { edges.push((i, j - 1)) };
        if j < w - 1 && map[i][j+1] >= lim { edges.push((i, j + 1)) };
        if i > 0 && map[i-1][j] >= lim { edges.push((i - 1, j)) };
        if i < h - 1 && map[i+1][j] >= lim { edges.push((i + 1, j)) };

        for (a, b) in edges {
            let new = cost + 1;

            if map[a][b] == 0 {
                println!("{:?}", new);
                // for row in map.iter() {
                //     println!("{:?}", row.iter().map(|x| x % 10).collect::<Vec<usize>>())
                // }
                // println!();
                // for row in dist.iter() {
                //     println!("{:?}", row.iter().map(|x| x % 10).collect::<Vec<usize>>())
                // }
                return;
            }

            if new < dist[a][b] {
                dist[a][b] = new;
                queue.push(Path{ cost: new, pos: (a,b) });
                // println!("{:?} {:?}", (a, b), dist[a][b]);
            }
        }
    }
}
