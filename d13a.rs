use std::{
    io::stdin,
};

#[derive(Debug)]
enum Value {
    List(Vec<Value>),
    Int(usize),
}

fn parse(s: &mut dyn std::iter::Iterator<Item=char>) -> Value {
    let mut vec = vec![];
    let mut cur = vec![];
    while let Some(c) = s.next() {
        match c {
            ',' | ']' => {
                if !cur.is_empty() {
                    let cc: String = cur.drain(..).collect();
                    let n: usize = cc.parse().unwrap();
                    vec.push(Value::Int(n));
                }
                if c == ']' { break };
            },
            '[' => vec.push( parse(s) ),
            n => cur.push(n),
        }
    }
    Value::List(vec)
}

fn main() {
    // let mut pairs = vec![];
    // let mut pair: Vec<String> = vec![];
    let lines: Vec<String> = stdin().lines().map(|l| l.unwrap()).collect();
    let pairs: Vec<[Value; 2]> = lines.chunks(3).map(|pair| 
            [
                parse(&mut pair[0].chars()),
                parse(&mut pair[1].chars()),
            ]
    ).collect();
    println!("{:?}", pairs[7]);
    // println!("{:?}", parse(&mut pairs[6][0].chars()));
}
