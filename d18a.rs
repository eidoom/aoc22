use std::{
    io::stdin,
    collections::HashSet,
};

fn main() {
    let mut outer = HashSet::new();

    for line in stdin().lines().map(|l| l.unwrap()) {
        let v: Vec<u32> = line.split(",").map(|x| x.parse().unwrap()).collect();

        let (x, y, z) = match v[..] {
            [x, y, z] => Some((x, y, z)),
            _ => None,
        }.unwrap();

        for face in [
            [x, y, z, 0],
            [x, y, z, 1],
            [x, y, z, 2],
            [x, y, z + 1, 0],
            [x, y + 1, z, 1],
            [x + 1, y, z, 2],
        ] {
            if !outer.remove(&face) {
                outer.insert(face);
            }
        }
    }

    println!("{}", outer.len());
}
