#!/usr/bin/env python3
"""
Run as:
```
./d01b_sql.py < i01 | sqlite3
```
"""

import functools, sys

if __name__ == "__main__":
    elf = 1
    data = []
    for line in sys.stdin:
        if line == "\n":
            elf += 1
        else:
            data.append(f"({elf}, {line.strip()})")

    print(
        """
CREATE TABLE calories (
    elf int NOT NULL,
    food int NOT NULL
);

INSERT INTO calories (elf, food) VALUES
    {};

SELECT SUM(total) FROM (
    SELECT SUM(food) as total FROM calories
    GROUP BY elf
    ORDER BY total DESC
    LIMIT 3
);
    """.format(
            ",\n    ".join(data)
        )
    )
