use std::{
    io::stdin,
    collections::VecDeque,
};

fn main () {
    let mut i0 = 0;
    let mut j0 = 0;

    let mut i1 = 0;
    let mut j1 = 0;

    let map: Vec<Vec<usize>> = stdin().lines().map(|l| l.unwrap()).enumerate().map(|(i, line)| {
        line.chars().enumerate().map(|(j, c)| {
            (match c {
                'S' => {
                    i0 = i;
                    j0 = j;
                    'a'
                },
                'E' => {
                    i1 = i;
                    j1 = j;
                    'z'
                },
                _  => c,
            }) as usize - 97
        }).collect()
    }).collect();

    let w = map[0].len();
    let h = map.len();

    // BFS
    let mut seen = vec![ [i0, j0] ];
    let mut queue = VecDeque::new();
    queue.push_back( [0, i0, j0] );

    while let Some( [cost, i, j] ) = queue.pop_front() {
        let lim = map[i][j] + 1;

        // Sadly generators are unstable
        // https://doc.rust-lang.org/stable/unstable-book/language-features/generators.html
        let mut edges = vec![];
        if j > 0 {
            edges.push( [i, j - 1] );
        };
        if j < w - 1 {
            edges.push( [i, j + 1] );
        };
        if i > 0 {
            edges.push( [i - 1, j] );
        };
        if i < h - 1 {
            edges.push( [i + 1, j] );
        };

        for [a, b] in edges {
            let new = cost + 1;

            if a == i1 && b == j1 {
                println!("{:?}", new);
                return
            }

            if !(map[a][b] > lim || seen.contains( &[a,b] )) {
                seen.push( [a, b] );
                queue.push_back( [new, a, b] );
            }
        }
    }
}
