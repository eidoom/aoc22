use std::{
    collections::HashSet,
    io::stdin,
};

// fn print(knots: &[[i32; 2]; 10], w: usize, h: usize)
// {
//     let mut out = vec![vec!['.'; w]; h];
//     for (i, knot) in knots.iter().rev().enumerate() {
//         out[h - 1 - knot[1] as usize][knot[0] as usize] = char::from_digit(9 - i as u32, 10).unwrap();
//     }
//     for line in out {
//         println!("{}", line.iter().collect::<String>());
//     }
//     println!();
// }

// fn print_end(poss: &HashSet<[i32; 2]>, w: usize, h: usize)
// {
//     let mut out = vec![vec!['.'; w]; h];
//     for &[x, y] in poss.iter() {
//         out[h - 1 - y as usize][x as usize] = '#';
//     }
//     for line in out {
//         println!("{}", line.iter().collect::<String>());
//     }
//     println!();
// }

fn main () {
    let mots: Vec<(char, usize)> = stdin().lines().map(|l| {
        let ll = l.unwrap();
        (
            ll.chars().next().unwrap(),
            ll[2..].parse().unwrap()
        )
    }).collect();

    let ww: Vec<i32> = mots.iter().scan(0, |x, &(m, n)| {
        match m {
            'L' => *x -= n as i32,
            'R' => *x += n as i32,
            _ => (),
        }
        Some(*x)
    }).collect();
    let l = *ww.iter().min().unwrap();
    // let r = *ww.iter().max().unwrap();
    // let w = (r - l + 1) as usize;
    let hh: Vec<i32> = mots.iter().scan(0, |y, &(m, n)| {
        match m {
            'U' => *y += n as i32,
            'D' => *y -= n as i32,
            _ => (),
        }
        Some(*y)
    }).collect();
    let d = *hh.iter().min().unwrap();
    // let u = *hh.iter().max().unwrap();
    // let h = (u - d + 1) as usize;

    let mut knots: [[i32; 2]; 10] = [[-l, -d]; 10]; // [x, y]
    let mut all = HashSet::from([knots[9]]);


    for &(m, n) in mots.iter() {
        let (i, s) = match m {
            'R' => Some((0,  1)),
            'L' => Some((0, -1)),
            'U' => Some((1,  1)),
            'D' => Some((1, -1)),
            _ => None,
        }.unwrap();

        for _ in 0..n {
            knots[0][i] += s;

            for j in 1..10 {
                let dx = knots[j - 1][0] - knots[j][0];
                let dy = knots[j - 1][1] - knots[j][1];
                let dxa = dx.abs();
                let dya = dy.abs();
                if (dxa > 1 && dya == 1)
                    || (dxa == 1 && dya > 1)
                    || (dxa == 2 && dya == 2) {
                        knots[j][0] += dx.signum();
                        knots[j][1] += dy.signum();
                }
                else if dxa > 1 {
                    knots[j][0] += dx.signum();
                }
                else if dya > 1 {
                    knots[j][1] += dy.signum();
                }
            }
            all.insert(knots[9]);
        }
        // print(&knots, w, h);
    }
    // print_end(&all, w, h);
    println!("{}", all.len());
}
