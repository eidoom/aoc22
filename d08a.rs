fn main () {
    let forest: Vec<Vec<u32>> = std::io::stdin().lines().map(|l|
        l.unwrap().chars().map(|c|
            c.to_digit(10).unwrap()
        ).collect()
    ).collect();

    let mut vis = 2 * (forest.len() + forest[0].len() - 2);
    for ii in 1..forest.len()-1 {
        for jj in 1..forest[ii].len()-1 {
            if forest[ii][..jj].iter().all(|&t| t < forest[ii][jj])
                || forest[ii][jj+1..].iter().all(|&t| t < forest[ii][jj])
                || forest[..ii].iter().map(|r| r[jj]).all(|t| t < forest[ii][jj])
                || forest[ii+1..].iter().map(|r| r[jj]).all(|t| t < forest[ii][jj]) {
                    vis += 1;
            }
        }
    }
    println!("{:?}", vis);
}
