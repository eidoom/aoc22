use std::io;

fn main () {
    let res: i32 = io::stdin().lines().map(|line| {
        let v: Vec<char> = line.unwrap().chars().collect();

        let opp = match v[0] {
            'A' => Some(1),
            'B' => Some(2),
            'C' => Some(3),
            _ => None,
        }.unwrap();

        match v[2] {
            'X' => Some(if opp - 1 == 0 { 3 } else { opp - 1 }), // lose
            'Y' => Some(3 + opp),                                // draw
            'Z' => Some(6 + opp % 3 + 1),                        // win
            _ => None,
        }.unwrap()
    }).sum();
    println!("{}", res);
}
