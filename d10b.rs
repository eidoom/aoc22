use std::{
    io::stdin,
};

fn draw(x: i32, cycle: usize) -> char {
    return if x == (cycle % 40) as i32 - 1 || x == (cycle % 40) as i32 + 0 || x == (cycle % 40) as i32 + 1 {
        '#'
    } else {
        '.'
    }
}

fn main () {
    let mut x = 1i32;
    let mut cycle = 0usize;
    let mut crt = [' '; 240];
    for line in stdin().lines().map(|l| l.unwrap()) {
        crt[cycle]=draw(x, cycle);
        let instr: Vec<&str> = line.split_ascii_whitespace().collect();
        cycle += 1;
        match instr[0] {
            "noop" => (),
            "addx" => {
                crt[cycle]=draw(x, cycle);
                x += instr[1].parse::<i32>().unwrap();
                cycle += 1;
            },
            _ => (),
        }
    }
    for l in 0..6 {
        println!("{}", &crt[40*l..40*(l+1)].iter().collect::<String>());
    }
}
