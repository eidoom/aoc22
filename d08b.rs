fn view<'a, I>(row: I, aim: u32) -> u32
where
    I: std::iter::Iterator<Item = &'a u32>,
{
    let mut i = 0;
    for tree in row {
        i += 1;
        if tree >= &aim {
            break;
        }
    }
    i
}

fn main () {
    let forest: Vec<Vec<u32>> = std::io::stdin().lines().map(|l|
        l.unwrap().chars().map(|c|
            c.to_digit(10).unwrap()
        ).collect()
    ).collect();

    let mut max = 0;
    for ii in 1..forest.len()-1 {
        for jj in 1..forest[ii].len()-1 {
            let cur = view(forest[ii][..jj].iter().rev(), forest[ii][jj])
                * view(forest[ii][jj+1..].iter(), forest[ii][jj])
                * view(forest[..ii].iter().map(|r| &r[jj]).rev(), forest[ii][jj])
                * view(forest[ii+1..].iter().map(|r| &r[jj]), forest[ii][jj]);
            if cur > max {
                max = cur;
            }
        }
    }
    println!("{:?}", max);
}
