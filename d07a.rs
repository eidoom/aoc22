fn main () {
    let mut stk = vec![];
    let mut cnt = 0;
    for line in std::io::stdin().lines() {
        let a = line.unwrap();
        let b: Vec<&str> = a.split_whitespace().collect();
        match b[0] {
            "$" => match b[1] {
                "cd" => match b[2] {
                    ".." => {
                        let m = stk.pop().unwrap();
                        if m <= 100000 {
                            cnt += m;
                        }
                        let l = stk.len() - 1;
                        stk[l] += m;
                    },
                    _ => stk.push(0),
                },
                "ls" => (),
                _ => (),
            },
            "dir" => (),
            _ => {
                let n: i32 = b[0].parse().unwrap();
                let l = stk.len() - 1;
                stk[l] += n;
            },
        }
    }
    println!("{}", cnt);
}
