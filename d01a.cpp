#include <iostream>
#include <string>

int main()
{
    int max {};
    int cur {};

    for (std::string line; std::getline(std::cin, line);) {
        if (line == "") {
            if (cur > max) {
                max = cur;
            }
            cur = 0;
        } else {
            int v = std::stoi(line);
            cur += v;
        }
    }

    std::cout << max << std::endl;
}
