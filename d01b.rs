use std::io;

fn main () {
    let lines = io::stdin().lines();
    let mut vec = vec![0];
    let mut pos = 0;
    for line in lines {
        match line.unwrap().as_str() {
            "" => {
                vec.push(0);
                pos += 1;
            },
            val => {
                vec[pos] += val.parse::<i32>().unwrap();
            },
        }
    }
    vec.sort_unstable();
    let res: i32 = vec.iter().rev().take(3).sum();
    println!("{}", res);
}
