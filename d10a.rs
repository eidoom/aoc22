use std::{
    io::stdin,
};

fn main () {
    let mut x = 1;
    let mut cycle = 0;
    let mut sum = 0;
    for line in stdin().lines().map(|l| l.unwrap()) {
        cycle += 1;
        if (cycle + 20) % 40 == 0 {
            // println!("{}: {} -> {}", cycle, x, cycle * x);
            sum += cycle * x;
        }
        let instr: Vec<&str> = line.split_ascii_whitespace().collect();
        // println!("{}", line);
        match instr[0] {
            "noop" => (),
            "addx" => {
                cycle += 1;
                if (cycle + 20) % 40 == 0 {
                    // println!("{}: {} -> {}", cycle, x, cycle * x);
                    sum += cycle * x;
                }
                x += instr[1].parse::<i32>().unwrap();
            },
            _ => (),
        }
    }
    println!("{}", sum);
}
