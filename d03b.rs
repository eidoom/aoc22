use std::{
    io,
    collections::HashSet
};

fn main () {
    println!("{}",
        io::stdin().lines().fold(
            (0, 0, HashSet::<char>::new()),
            |(sum, cnt, mut com), line| {
                let val: HashSet<char> = line.unwrap().chars().collect();
                com = if cnt == 0 { val } else { com.intersection(&val).copied().collect() };
                (
                    sum + if cnt == 2 { match com.iter().next() {
                        Some(&a @ 'a'..='z') => Some(a as i32 - 96),
                        Some(&a @ 'A'..='Z') => Some(a as i32 - 38),
                        _ => None,
                    }.unwrap()} else { 0 },
                    (cnt + 1) % 3,
                    com,
                )
            }).0
    );
}
