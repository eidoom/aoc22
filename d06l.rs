use std::io;

pub fn find_marker(n: usize) -> Option<usize> {
    let mut buf = String::new();
    io::stdin().read_line(&mut buf).unwrap();
    let line: Vec<char> = buf.chars().collect();

    'outer: for (i, win) in line.windows(n).enumerate() {
        // we set up a binary number big enough to map to the (lowercase) alphabet
        let mut bitmask = 0u32;
        for c in win.iter() {
            // we map each char to a position in the bitmask 
            // so we can represent it with a binary number,
            // constructed with a bitwise left shift (so most significant bit first ordering)
            // eg 'b' gets position 2, so the binary number is 0000000000000000000000000000010
            let b = 1 << (*c as u8 - 97);
            // the bitwise-and tells us the letter is already in our bitmask if the result is nonzero
            if bitmask & b != 0 {
                // so we have a duplicate letter and move to the next window
                continue 'outer;
            }
            // otherwise we use a bitwise-or to add the letter to the bitmask
            bitmask |= b;
        }
        // if no duplicates were found, we return the position as the solution
        return Some(i + n);
    }
    None
}
