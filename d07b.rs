fn main () {
    let mut stk = vec![];
    let mut cnt = vec![];
    for line in std::io::stdin().lines() {
        let a = line.unwrap();
        let b: Vec<&str> = a.split_whitespace().collect();
        match b[0] {
            "$" => match b[1] {
                "cd" => match b[2] {
                    ".." => {
                        let m = stk.pop().unwrap();
                        cnt.push(m);
                        let l = stk.len() - 1;
                        stk[l] += m;
                    },
                    _ => stk.push(0),
                },
                "ls" => (),
                _ => (),
            },
            "dir" => (),
            _ => {
                let n: i32 = b[0].parse().unwrap();
                let l = stk.len() - 1;
                stk[l] += n;
            },
        }
    }
    let end: Vec<i32> = stk.iter().rev().scan(0, |acc, &x| {
        *acc = *acc + x;
        Some(*acc)
    }).collect();
    cnt.extend(end);
    cnt.sort_unstable();
    let need = cnt.pop().unwrap() - 40000000;
    let sol = cnt.iter().find(|&&x| x >= need).unwrap();
    println!("{:?}", sol);
}
